module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:storybook/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:unicorn/recommended',
    'xo',
    'xo-typescript',
    'prettier',
  ],
  plugins: ['@typescript-eslint', 'svelte3'],
  settings: {
    'svelte3/typescript': () => require('typescript'),
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: 'tsconfig.eslint.json',
    extraFileExtensions: ['.svelte'],
  },
  rules: {
    '@typescript-eslint/naming-convention': 'off',
    'capitalized-comments': 'off',
    curly: ['error', 'multi-or-nest', 'consistent'],
    'new-cap': 'off',
    'unicorn/no-abusive-eslint-disable': 'off',
    'unicorn/prevent-abbreviations': [
      'error',
      { replacements: { args: false, i: false, j: false } },
    ],
  },
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3',
      rules: {
        '@typescript-eslint/no-unsafe-assignment': 'off',
        '@typescript-eslint/no-unsafe-call': 'off',
        '@typescript-eslint/no-unused-vars': 'off',
      },
    },
  ],
  ignorePatterns: ['**/build/', '*.cjs'],
}
