declare module '@bonfire/pwa' {
  export const handler: (
    request: unknown,
    response: unknown,
    next: unknown
  ) => void
}
