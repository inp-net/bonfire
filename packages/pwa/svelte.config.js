/* eslint-disable */
import adapter from '@sveltejs/adapter-node'
import globalConfig from '../../svelte.config.js'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  ...globalConfig,

  kit: {
    adapter: adapter(),
    vite: {
      server: {
        port: 7777,
        proxy: { '/api': { target: 'http://localhost:3000', ws: true } },
      },

      css: {
        preprocessorOptions: {
          scss: {
            additionalData: `@use "${
              new URL('src/variables.scss', import.meta.url).pathname
            }" as *;`,
          },
        },
      },
    },
  },
}

export default config
