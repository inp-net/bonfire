import type { Handle } from '@sveltejs/kit'

const apiOrigin = process.env.API_ORIGIN ?? `http://localhost:3000`

export const handle: Handle = async ({ event, resolve }) => {
  if (event.url.pathname.startsWith('/api')) {
    return fetch(
      new URL(event.url.pathname, apiOrigin).toString(),
      event.request
    )
  }

  return resolve(event)
}
