import { defineConfig } from 'vite'
import { VitePluginNode } from 'vite-plugin-node'
import tsconfig from './tsconfig.json'
import { generate } from '@bonfire/api-client/generate'

export default defineConfig({
  server: {
    port: 3000,
    strictPort: true,
  },
  optimizeDeps: {
    // Vite does not work well with optionnal dependencies,
    // mark them as ignored for now
    exclude: [
      '@nestjs/microservices',
      '@nestjs/websockets',
      'cache-manager',
      'class-transformer',
      'class-validator',
      'fastify-swagger',
    ],
  },
  // `vite build` settings
  build: {
    outDir: tsconfig.compilerOptions.outDir,
    target: 'esnext',
    lib: { entry: 'index.ts', formats: ['es'] },
  },
  plugins: [
    // This plugin refreshes `api-client/index.ts` on every file change
    {
      name: 'api-client-generator',
      configureServer(vite) {
        const refreshApiClient = async () => {
          console.time('api-client-generator')
          const { document } = (await vite.ssrLoadModule(
            './index.ts'
          )) as typeof import('./index.js')
          await generate(await document)
          console.timeEnd('api-client-generator')
        }

        void refreshApiClient()
        vite.watcher.on('all', refreshApiClient)
      },
    },
    VitePluginNode({
      adapter: 'nest',
      appPath: './index.ts',
      exportName: 'api',
    }),
  ],
})
