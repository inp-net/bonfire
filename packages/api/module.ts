import AccountsModule from '@bonfire/accounts-backend'
import PingModule from '@bonfire/ping-backend'
import { Module } from '@nestjs/common'
import { LoggerModule } from 'nestjs-pino'

@Module({
  imports: [AccountsModule, PingModule, LoggerModule.forRoot()],
})
export default class ApiModule {}
