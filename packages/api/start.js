import { api } from './build/index.js'

const port = Number(process.env.PORT ?? 3000)
void api
  .then(async (api) => api.listen(port))
  .then(() => {
    console.log(`Listening on 0.0.0.0:${port}`)
  })
