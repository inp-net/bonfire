import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { Logger } from 'nestjs-pino'
import ApiModule from './module.js'

export const createApi = async () => {
  const api = await NestFactory.create(ApiModule, { bufferLogs: true })
  api.useLogger(api.get(Logger))
  api.setGlobalPrefix('api')
  api.enableCors()

  const config = new DocumentBuilder()
    .setTitle('Bonfire API')
    .setDescription('🔥')
    .setVersion('1.0')
    .build()
  const document = SwaggerModule.createDocument(api, config)
  SwaggerModule.setup('api', api, document)

  return { api, document }
}

const output = createApi()

export const api = output.then(({ api }) => api)
export const document = output.then(({ document }) => document)

export { default as BackendModule } from './module.js'
