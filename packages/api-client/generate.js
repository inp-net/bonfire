#!/usr/bin/env node
import { mkdir, writeFile } from 'node:fs/promises'
import { generateSource } from 'oazapfts/lib/codegen/index.js'

/** @param {import('@nestjs/swagger').OpenAPIObject} document */
export const generate = async (document) => {
  const code = await generateSource(document, { optimistic: true })
  // Because we use ESM (EcmaScript Modules) and not CJS (Common JavaScript modules),
  // we need to patch the resulting code to add full-path imports.
  const patched =
    '/* eslint-disable */\n' +
    code
      .replace('oazapfts/lib/runtime', 'oazapfts/lib/runtime/index.js')
      .replace('oazapfts/lib/runtime/query', 'oazapfts/lib/runtime/query.js')
  await mkdir(new URL('build', import.meta.url), { recursive: true })
  await writeFile(new URL('build/index.ts', import.meta.url), patched)
}

if (process.argv[1] === new URL(import.meta.url).pathname) {
  import('@bonfire/api')
    .then(async ({ document }) => document)
    .then(generate)
    .catch((error) => {
      console.error(error)
      process.exit(1)
    })
}
