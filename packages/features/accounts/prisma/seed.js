// @ts-check
import { PrismaService } from '@bonfire/prisma-client'

const prisma = new PrismaService()

const seed = async () => {
  const wishtitute = await prisma.school.create({
    data: {
      name: 'Wishtitute',
      majors: {
        create: [
          {
            name: 'Philosophy',
          },
          {
            name: 'Satanism',
          },
        ],
      },
    },
    include: { majors: true },
  })

  await prisma.user.create({
    data: {
      firstName: 'Alice',
      middleName: '',
      lastName: 'Dev',
      birthday: new Date('2000-04-01T00:00:00Z'),
      cri: 'adev',
      email: 'alice@example.com',
      entryYear: 2020,
      graduationYear: 2023,
      majorId: wishtitute.majors[0].id,
    },
  })
  await prisma.user.create({
    data: {
      firstName: 'Bob',
      middleName: 'Le Daron',
      lastName: 'Dev',
      birthday: new Date('1970-01-01T00:00:00Z'),
      cri: 'bdev',
      email: 'bob@example.com',
      entryYear: 1990,
      graduationYear: 1995,
      majorId: wishtitute.majors[1].id,
    },
  })
}

seed().catch((error) => {
  console.error(error)
})
