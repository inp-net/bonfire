import { PrismaService } from '@bonfire/prisma-client'
import { Module } from '@nestjs/common'
import { Accounts } from './accounts.controller.js'

@Module({
  controllers: [Accounts],
  providers: [PrismaService],
})
export default class AccountsModule {}
