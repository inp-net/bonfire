import { PrismaService } from '@bonfire/prisma-client'
import { Controller, Get, Query } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

@Controller('accounts')
@ApiTags('accounts')
export class Accounts {
  constructor(private readonly prisma: PrismaService) {}

  @Get('search') search(@Query('q') q: string) {
    const terms = new Set(String(q).split(' ').filter(Boolean))
    const years = new Set(
      [...terms].filter((term) => /^\d+$/.test(term)).map(Number)
    )
    for (const year of years) terms.delete(String(year))
    const search = [...terms].join('&')

    return this.prisma.user.findMany({
      where: {
        ...(terms.size > 0
          ? {
              firstName: { search },
              middleName: { search },
              lastName: { search },
              cri: { search },
              email: { search },
            }
          : {}),
        ...(years.size > 0
          ? {
              OR: [
                { entryYear: { in: [...years] } },
                { graduationYear: { in: [...years] } },
              ],
            }
          : {}),
      },
    })
  }
}
