import { Controller, Get, Logger } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

@Controller('ping')
@ApiTags('ping')
export default class Ping {
  private readonly logger = new Logger(Ping.name)

  /** Returns `pong`. */
  @Get() pong() {
    this.logger.log('ping')
    return 'pong'
  }
}
