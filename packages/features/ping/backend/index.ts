import { PrismaService } from '@bonfire/prisma-client'
import { Module } from '@nestjs/common'
import Controller from './controller.js'

@Module({
  controllers: [Controller],
  providers: [PrismaService],
})
export default class PingModule {}
