import { api } from '@bonfire/api'
import { handler } from '@bonfire/pwa'

/** @param {import('@nestjs/core').NestApplication} api */
const main = async (api) => {
  api.getHttpAdapter().setNotFoundHandler = undefined
  await api.init()
  api.use(handler)
  const { port } = new URL(process.env.API_ORIGIN)
  await api.listen(port)
  console.log(`Listening on http://localhost:${port} ...`)
}

api.then(main).catch((error) => {
  console.error(error)
})
