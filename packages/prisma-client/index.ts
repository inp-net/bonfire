import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common'
import client from '@prisma/client'
export * from '@prisma/client'

/** An injectable and singleton prisma client. */
@Injectable()
export class PrismaService
  extends client.PrismaClient
  implements OnModuleInit, OnModuleDestroy
{
  constructor() {
    super({ log: ['query'] })
  }

  async onModuleInit() {
    await this.$connect()
  }

  async onModuleDestroy() {
    await this.$disconnect()
  }
}
