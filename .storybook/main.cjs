/** @type {import('@storybook/core-common').StorybookConfig} */
module.exports = {
  stories: ['../**/*.stories.svelte'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-svelte-csf',
  ],
  framework: '@storybook/svelte',
  core: { builder: '@storybook/builder-vite' },
  svelteOptions: import('../svelte.config.js'),
}
