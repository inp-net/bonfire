FROM debian:bullseye-slim
WORKDIR /bonfire

# Install Volta
ENV VOLTA_HOME=/root/.volta
ENV PATH=$VOLTA_HOME/bin:$PATH
RUN set -eux; \
	apt-get update && apt-get install -y curl; \
	curl https://get.volta.sh | bash -s -- --skip-setup

# Build the whole monorepo
COPY . .
RUN yarn install --immutable && yarn build

EXPOSE 7777
STOPSIGNAL SIGINT
CMD yarn prisma migrate reset --force && yarn start
