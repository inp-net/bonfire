# 🔥 Bonfire

A [Progressive Web App](https://web.dev/what-are-pwas/) for the schools of ~~Centrale Toulouse~~ Toulouse INP.

## Quick start

The whole project is designed to run (in development mode) in a single container.

```bash
docker build . --tag bonfire:latest
docker run -it -p 7777:7777 bonfire:latest
# Open http://localhost:7777
```

_We'll move to docker-compose in the future._

## Architecture

### A monorepo

> A monorepo is a single repository containing multiple distinct projects, with well-defined relationships.
> – [monorepo.tools](https://monorepo.tools/#what-is-a-monorepo)

Bonfire is a monorepo containing:

- Individual services (accounts, ecobox, loca7...)
- Common utilities (prisma...)
- The application code (backend and frontend)
- ...

All these packages are interdependent, and together form a dependency tree like this one:

```mermaid
graph TD
  subgraph accounts
    accounts-backend
    accounts-frontend
  end
  subgraph ecobox
    ecobox-backend
    ecobox-frontend
  end
  api --> accounts-backend --> prisma
  api --> ecobox-backend --> prisma
  pwa --> accounts-frontend --> api-client
  pwa --> ecobox-frontend --> api-client
  api-client --> api
  main --> api
  main --> pwa
```

A dependency graph can be produced with the command `yarn turbo run build --graph`.

It reads as follows: _main depends on api-server_.

### Services

A service is made of:

- A backend, that defines API endpoints
- A frontend, that defines many frontend components
- A database schema and a seeding functions, used to store and generate date

## Differences with _Portail_

There are many fundamental differencies with [INP-net/Portail](https://git.inpt.fr/inp-net/portail) that explain this complete rewrite:

- An API-first approach: the old portal has tightly coupled back and frontends, making it impossible for other services to use its data
- A modern frontend: the old portal frontend is written with [Vanilla JS](http://vanilla-js.com/) and jQuery, making it impossible to create complex client-side applications
- A typesafe code: Bonfire is written in [TypeScript](https://www.typescriptlang.org/), and even the API is fully typed
- A modular approach: Bonfire is written as a collection of interdependent services rather than an MVC monolith

## Development mode

You'll need to install [Volta](https://volta.sh/) (a tool to manage Node and Yarn versions) and restart your shell before continuing.

```bash
# Clone the repo
git clone https://git.inpt.fr/inp-net/bonfire
cd bonfire

# Install the dependencies
yarn install

# Build the project
yarn build

# Development mode
yarn prisma migrate dev
yarn dev # Starts the API on http://localhost:3000/ and the PWA on http://localhost:7777/

# Production mode
yarn prisma migrate deploy
yarn start # Starts on http://localhost:7777/
```

## Licence

[AGPL-3.0](./LICENSE)
