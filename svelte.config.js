import preprocess from 'svelte-preprocess'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess({
    scss: {
      prependData: `@use "${
        new URL('packages/pwa/src/variables.scss', import.meta.url).pathname
      }" as *;`,
    },
  }),
}

export default config
